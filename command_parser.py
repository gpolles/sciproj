__author__ = 'gpolles'
from getpass import getpass
from core.script import Script
from logging import error
from copy import copy
import numpy


class CommandParser:
    def __init__(self, pm=None, sm=None, wm=None, jm=None, js=None):
        self.pm = pm
        self.sm = sm
        self.wm = wm
        self.jm = jm
        self.js = js

    def graceful_exit(self):
        self.wm.save_state()
        self.pm.save_state()
        self.sm.save_state()
        self.jm.save_state()
        if self.js.dispatcher_thread is not None:
            print 'waiting for dispatcher to end.. '
            self.js.msg.put('stop')
            self.js.dispatcher_thread.join()
        print "Bye."
        exit(0)

    def parse_command(self, cmd):
        commands = {
            ('add', 'project'): self.add_project,
            ('add', 'script'): self.add_script,
            ('add', 'worker'): self.add_worker,
            ('list', 'projects'): self.list_projects,
            ('list', 'scripts'): self.list_scripts,
            ('list', 'workers'): self.list_workers,
            ('select', 'project'): self.select_project,
            ('run', 'script'): self.run_script,
            ('list', 'jobs'): self.list_jobs,
            ('run', 'dispatcher'): self.js.run_dispatcher,
            ('del', 'script'): self.del_script,
            ('add', 'parameter'): self.add_parameter,
            ('list', 'parameters'): self.list_parameters,
            ('list', 'branches'): self.list_branches,
            ('add', 'branch'): self.add_branch
        }

        if cmd == 'q':
            self.graceful_exit()

        try:
            commands[tuple(cmd.split())]()
        except KeyError:
            print 'command not recognized'

    def add_parameter(self):
        p = self.pm.active_project
        if p is None:
            print 'No active project'
            return
        parameter = raw_input("> Parameter name: ")
        if parameter in p.parameters:
            print('parameter ' + parameter + ' is already set.')
            return
        if len(parameter) == 0:
            print('you need to specify a name for the parameter.')
            return
        default_value = raw_input("> Parameter default value: ")

        p.addParameter(parameter, default_value)

        # update old jobs with the default value - is this wanted?
        for j in self.jm.jobs:
            if j.project == p.name:
                if parameter not in j.parameters.keys():
                    j.parameters[parameter] = default_value

    def list_scripts(self):
        print '*'*35 + ' Scripts  ' + '*'*35
        for i, s in enumerate(self.sm.scripts):
            print "{:4d} : {:30s} : {:30s}".format(i, s.path, s.desc)
        print '*'*80

    def del_script(self):
        self.list_scripts()
        try:
            i = int(raw_input('Remove which script (-1 to cancel)?'))
        except ValueError:
            error('invalid id')
            return
        if i == -1:
            return
        self.sm.del_script(i)

    def add_script(self):
        s = Script()
        desc = raw_input("Description: ")
        s.desc = desc
        path = raw_input("Path: ")
        if not s.load(path):
            return False
        # TODO: project/branch/type
        # r = raw_input("Assign to a specific project/branch? (y/n): ")
        # if r == 'y' or r == 'Y':
        cl = ''
        while cl != 'i' and cl != 'p' and cl != 'a':
            cl = raw_input('Script type - (i)nitialization, (p)roduction, (a)nalysis :')
        if cl == 'i':
            s.script_class = 'init'
        if cl == 'p':
            s.script_class = 'production'
        if cl == 'a':
            s.script_class = 'analysis'

        path = raw_input('Path for a status script: ')
        if not s.load_status_script(path):
            print 'could not load a status script.'
        self.sm.addScript(s)
        self.sm.saveState()

    def add_worker(self):
        server = raw_input("Server: ")
        user = raw_input("Username: ")
        print "Password: "
        pwd = getpass()
        scratch_dir = raw_input("Scratch directory: ")
        path = raw_input('Path for a status script: ')
        self.wm.addWorker(server, user, pwd, scratch_dir, path)
        self.wm.saveState()

    def list_workers(self):
        print '*'*35 + ' Workers  ' + '*'*35
        for i, w in enumerate(self.wm.workers):
            print "{:4d} : {:30s} : {:30s}".format(i, w.server, w.status)
        print '*'*80

    def list_parameters(self):
        p = self.pm.active_project
        if p is None:
            print 'No active project'
            return
        for i, p in enumerate(p.parameters):
            print "{:4d} : {:30s}".format(i, p)

    def list_branches(self):
        p = self.pm.active_project
        if p is None:
            print 'No active project'
            return
        for i, p in enumerate(p.branches):
            print "{:4d} : {:30s}".format(i, p)

    def add_branch(self):
        p = self.pm.active_project
        if p is None:
            print 'No active project'
            return
        branch = raw_input('New branch name: ')
        if branch in p.branches:
            print 'branch ' + branch + ' is already present.'
            return
        if len(branch) == 0:
            print 'you need to specify a name for the branch.'
            return
        p.add_branch(branch)

    def list_jobs(self):
        print '*'*37 + ' Jobs ' + '*'*37
        for i, job in enumerate(self.jm.jobs):
            tmp_params = copy(job.parameters)
            del tmp_params['ID']
            print "{:4d} : {:10s} : {:30s} : {:20s}".format(i, job.project, tmp_params, job.status)
        print '*'*80

    def run_script(self):
        prj = self.pm.active_project
        if prj is None:
            print 'No active project'
            return

        # choose the script
        self.list_scripts()
        script_id = raw_input('which script do you want to run?')
        script = self.sm.get_script(int(script_id))

        # choose the workers
        self.wm.listWorkers()
        wstr = raw_input('On which workers do you want to run on? (space separated ids)')
        workers = []
        for w in wstr.split():
            workers.append(self.wm.getWorker(int(w)))

        # TODO: check which parameters the script requires

        # Choose parameters
        # Choose parameter space
        print 'Choose the values. You can either insert a list of values separated by spaces,'
        print 'a single value or use the command "!range <min> <max> <step>"'

        param_space = []
        for p in self.pm.prj.parameters:
            ok = False
            while not ok:
                print p
                range_str = raw_input('Values for ' + p + ': ')
                values = range_str.split()
                if values[0] == '!range':
                    values = numpy.arange(float(values[1]),
                                          float(values[2])+float(values[3])/2,
                                          float(values[3]))
                if len(values) != 0:
                    param_space.append([p, values])
                    ok = True
                else:
                    print 'At least one value for ' + p + 'is needed.'
                    ok = False

        num_jobs = 1
        for p in param_space:
            num_jobs *= len(p[1])

        print
        print 'Review your submission:'
        print '*'*80
        print '>> Script: \n' + script.desc
        print '>> Workers:'
        for w in workers:
            print w.server
        print '>> Parameters:'
        for p in param_space:
            print p[0], '>', p[1]
        print '>> Total jobs: ', num_jobs
        print '*'*80
        accept = raw_input('Continue (y/n)? ')
        if accept != 'y' and accept != 'Y':
            print 'canceled.'
            return
        self.jm.submit_block(script, workers, prj, param_space)

    def add_project(self):
        print "--New project-- (Leave name empty to cancel)"
        name = ''
        already_in_use = True
        while already_in_use:
            name = raw_input("Project name: ")
            if len(name) == 0:
                print 'canceled.'
                return
            already_in_use = False
            for p in self.pm.projects:
                if p.name == name:
                    print ' > Error: Name already in use.'
                    already_in_use = True
                    break
        desc = raw_input("Project description: ")
        self.pm.add_project(name, desc)

    def list_projects(self):
        print '*'*35 + ' Projects ' + '*'*35
        print "  id  | name                           | description"
        for i, p in enumerate(self.pm.projects):
            c = ' '
            if p == self.pm.active_project:
                c = '+'
            print "{:4d} {:s} : {:30s} : {:30s}".format(i, c, p.name, p.description)
        print '*'*80

    def select_project(self):
        self.list_projects()
        i = int(raw_input('Select project number: '))
        if i not in range(0, len(self.pm.projects)):
            print 'invalid id.'
            return
        self.pm.select_project(i)
