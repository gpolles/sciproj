__author__ = 'gpolles'

from core.worker import Worker

from script_manager import ScriptManager
from project_manager import ProjectManager
from worker_manager import WorkerManager
from job_manager import JobManager

from command_parser import CommandParser
from job_scheduler import JobScheduler
import getpass
import os
import ConfigParser

# if __name__=='__main__':
if True:
    base_path = os.path.expanduser('~/SciProj')
    config = ConfigParser.ConfigParser()
    try:
        config.readfp(os.path.expanduser('~/.sciproj.cfg'))
    except:
        config.add_section('init')
        config.set('init', 'path', os.path.expanduser('~/SciProj'))
        with open(os.path.expanduser('~/.sciproj.cfg'), 'w') as configfile:
            config.write(configfile)
    base_path = config.get('init', 'path')

    if Worker.encryption_pwd == '':
        print ' > Please provide your password for storing access passwords to workers:'
        Worker.encryption_pwd = getpass.getpass()

    if os.path.exists(base_path):
        os.chdir(base_path)
    else:
        os.makedirs(base_path)
        os.chdir(base_path)

    print os.getcwd()

    sm = ScriptManager()
    pm = ProjectManager()
    wm = WorkerManager()
    jm = JobManager()

    js = JobScheduler(sm=sm, pm=pm, wm=wm, jm=jm)

    command_parser = CommandParser(pm=pm, sm=sm, wm=wm, js=js, jm=jm)

    js.run_dispatcher()

    while True:
        cmd = raw_input(" > ")
        command_parser.parse_command(cmd)
