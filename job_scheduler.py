__author__ = 'gpolles'

import sys
from Queue import Queue, Empty
from core.script import parse_template
from threading import Thread
from spur import RunProcessError
from logging import debug, error
from core.worker import Worker

class JobScheduler:
    def __init__(self, pm=None, sm=None, wm=None, jm=None):
        self.pm = pm
        self.sm = sm
        self.wm = wm
        self.jm = jm
        self.msg = Queue()
        self.dispatcher_thread = None
        self.status = ''

    def run_job(self, job, worker):
        j = job
        w = worker
        j.assigned_to = w

        if w is None:
            raise NameError("cannot run a job without a worker!")
        shell = w.getShell()

        if j.project == '':
            print 'No project specified.'
            j.status = 'failed'
            return

        try:
            try:
                print 'making directories if necessary - ' + w.scratch_dir + '/' + j.project
                shell.run(['mkdir', j.project], cwd=w.scratch_dir)
            except RunProcessError as e:
                print e.message

            try:
                print 'making directories if necessary - ' + w.scratch_dir + '/' + j.project + '/' + j.project_branch
                if j.project_branch != '':
                    shell.run(['mkdir', j.project_branch], cwd=w.scratch_dir + '/' + j.project)
            except RunProcessError as e:
                print e.message

            try:
                j.remote_dir = '/'.join([w.scratch_dir,
                                         j.project,
                                         j.project_branch,
                                         str(j.unique_id)])
                print 'making directories if necessary - ' + j.remote_dir
                shell.run(['mkdir', j.remote_dir])
            except RunProcessError as e:
                print e.message

            # try to copy script

            debug('copying script..')
            script_path = j.remote_dir + '/' + str(j.unique_id) + '.job'
            try:
                script = parse_template(self.sm.getScriptByUUID(j.template_script).content,
                                        j.parameters)
                with shell.open( script_path, "w") as remote_file:
                    remote_file.write(script)
                shell.run(["chmod", "u+x", script_path])

                script = parse_template(self.sm.getScriptByUUID(j.template_script).status_script,
                                        j.parameters)
                with shell.open(job.remote_dir + '/pj_status_script.tmp', 'w') as remote_file:
                    remote_file.write(script)
                shell.run(['chmod', 'u+x', job.remote_dir + '/pj_status_script.tmp'])
            except RunProcessError as e:
                error(str(e))
                raise e

            # try to launch the script, using nohup or screen
            # nohup ./myprogram > foo.out 2> foo.err < /dev/null &
            # or
            # screen -A -m -d -S somename ./somescript.sh &
            try:
                cmd = Worker.spawn_cmd[w.system].format(cmd=script_path, job=j.unique_id)
                shell.unsafe_spawn(cmd, cwd=j.remote_dir)
                debug('job sent.')
                j.status = 'sent to worker'
                j.is_running = True
            except RunProcessError as e:
                print e.message
                raise
            except KeyError:
                error('Invalid system.')


        except:
            j.status = 'failed'
            error('job running on ' + w.server + ' failed')
            error(str(sys.exc_info()[0]))

    def check_job_status(self, job, reload_script=True):
        if job.status == 'completed':
            return 'completed'
        w = job.assigned_to
        if w is None:
            raise RuntimeError('cannot check a status without a worker!')
        shell = w.getShell()
        result = None
        error_flag = False
        try:
            if reload_script:
                # try to copy script
                script = parse_template(self.sm.getScriptByUUID(job.template_script).status_script,
                                        job.parameters)
                with shell.open(job.remote_dir + '/pj_status_script.tmp', 'w') as remote_file:
                    remote_file.write(script)
                shell.run(['chmod', 'u+x', job.remote_dir + '/pj_status_script.tmp'])
            result = shell.run(['./pj_status_script.tmp'], cwd=job.remote_dir)
        except RunProcessError as e:
            job.status = 'unknown'
            print e.message
            error_flag = True
        if len(result.output) == 0:
            job.status = 'unknown'
            print 'No output from the status script for job ' + str(job.unique_id)
            error_flag = True

        job.is_running = False
        if not error_flag:
            general_status = result.output.split()[0]
            if general_status == 'running':
                job.is_running = True
            job.status = result.output
        return job.status

    def check_worker_status(self, worker, reload_script=True):
        w = worker
        if reload_script:
            w.uploadStatusScript()
        return w.getStatus()

    def cancel_job(self,job):
        w = self.wm.get_worker_by_uuid(job.assigned_to)
        if w is None:
            error('no valid worker for job ' + str(job.unique_id) )
        try:
            shell = w.get_shell()
            cmd = Worker.del_cmd[w.system].format(job=job.unique_id)
            shell.unsafe_run(cmd)
        except:
            error('cancel_job() error: %s' % sys.exc_info()[0])

    # TODO: dispatcher needs to check workers as well
    def dispatcher(self):
        sleep_time = 1
        while True:
            msg = ''
            try:
                msg = self.msg.get(True, sleep_time)
            except Empty:
                pass
            if msg == 'stop':
                return

            # update job status
            for job in self.jm.jobs:
                if job.is_running:
                    if self.check_msg() == 'stop':
                        return
                    self.check_job_status(job, reload_script=False)

            # check workers status
            for w in self.wm.workers:
                if self.check_msg() == 'stop':
                    return
                self.check_worker_status(w, reload_script=False)

            for job in self.jm.jobs:
                # find the first queued job
                if job.status == 'queued':
                    # tries to run the job on a free worker node
                    free_worker = None
                    for worker_id in job.candidate_workers:
                        w = self.wm.get_worker_by_uuid(worker_id)
                        if w.status == 'idle':
                            free_worker = w
                            break
                    if free_worker is None:
                        debug('no available workers')
                    else:
                        self.run_job(job, free_worker)
                        break

    def check_msg(self):
        try:
            msg = self.msg.get(False)
            if msg == 'stop':
                return 'stop'
            if msg == 'pause':  # hangs until a continue is issued
                while True:
                    try:
                        msg = self.msg.get(True, 0.1)
                        if msg == 'stop':
                            return 'stop'
                        if msg == 'continue':
                            return 'continue'
                    except Empty:
                        pass

        except Empty:
            pass
        return 'continue'

    def run_dispatcher(self):
        self.dispatcher_thread = Thread(target=self.dispatcher)
        self.dispatcher_thread.start()
