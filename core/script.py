__author__ = 'gpolles'
import uuid
import re
from logging import error,debug


def parse_template(template, parameters):
    p = re.compile('\{!![a-zA-Z0-9_]+!!\}')
    keys = p.findall(template)
    rest = p.split(template)
    if template[:3] == "{!!":
        rest.insert(0, '')  # if script starts with a key
    for i, key in enumerate(keys):
        key = key[3:-3]
        try:
            rest[i] += str(parameters[key])
        except:
            raise NameError('parameter "' + key + '" in template not set ')
    return unicode(''.join(rest))


class Script:
    def __init__(self, path='', description='', project='', branch='', script_class='', status_path=''):
        self.uuid = unicode(uuid.uuid4())
        self.path = path
        self.status_path = status_path
        self.status_script = ''
        self.description = description
        self.project = project
        self.branch = branch
        self.script_class = script_class  # 'init' 'production' 'analysis'
        self.content = ''
        if self.path != '':
            self.load(self.path)
        if self.status_path != '':
            self.load_status_script(self.status_path)

    def load(self, path):
        try:
            fin = open(path, "r")
            self.content = fin.read()
            fin.close()
            self.path = path
            return True
        except:
            error('Error: Cannot load file ' + self.path)
            return False

    def load_status_script(self, path):
        try:
            fin = open(path, 'r')
            self.status_script = fin.read()
            fin.close()
            self.status_path = path
            return True
        except:
            error('Error: Cannot load file ' + path)
            return False