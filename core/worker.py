__author__ = 'gpolles'
import spur
import spur.ssh
import sys
from logging import error
from uuid import uuid4
from my_ssh_shell import MySshShell


class Worker:

    spawn_cmd = {'screen': 'screen -m -d -S {job} {cmd}',
                 'nohup': 'nohup {cmd} &',
                 'pbs': 'qsub {cmd}'}
    del_cmd = {'screen': 'screen -X -S {job} quit',
               'nohup': 'kill `ps auxww | grep {job} | grep -v grep | awk \'{{print $2}}\' `',
               'pbs' : 'qdel `qstat -w | grep {job} |awk \'{{print $1}}\` '}

    # this should be modified at runtime, to manage all workers
    encryption_pwd = ''

    def __init__(self):
        self.server = ''
        self.unique_id = unicode(uuid4())
        self.username = ''
        self.password = ''
        self.scratch_dir = ''
        self.storage_dir = ''
        self.status_script = ''
        self.upload_script = True
        self.active = True
        self.status = 'unknown'
        self.system = 'screen'

    def get_shell(self):
        shell = MySshShell(hostname=self.server,
                           username=self.username,
                           password=self.password,
                           missing_host_key=spur.ssh.MissingHostKey.accept)
        return shell

    def check_configuration(self):
        shell = self.get_shell()
        try:
            result = shell.unsafe_run('ls', cwd=self.scratch_dir)
        except:
            e = sys.exc_info()[0]
            print "something went wrong"
            print str(e)
            return False
        if result.return_code != 0:
            print "something went wrong"
            print result.stderr_output
            return False
        print 'Your scratch: '
        print result.output
        print 'ALL OK'
        return True

    def upload_status_script(self):
        try:
            shell = self.get_shell()
            with shell.open(self.scratch_dir + '/pj_worker_status_script.tmp', "w") as remote_file:
                remote_file.write(unicode(self.status_script))
            shell.run(["chmod", "u+x", './pj_worker_status_script.tmp'], cwd=self.scratch_dir)
            return True
        except spur.RunProcessError as e:
            error(e.stderr_output)
        except spur.NoSuchCommandError as e:
            error('could not find command: ' + e.command)
        except:
            error(str(sys.exc_info()[0]))
        return False

    def update_status(self):
        try:
            shell = self.get_shell()
            result = shell.run(['./pj_worker_status_script.tmp'], cwd=self.scratch_dir)
        except:
            self.status = 'down'
            error('exception caught while trying to execute remote worker script: ' + str(sys.exc_info()[0]))
            return self.status
        if len(result.output) == 0:
            self.status = 'down'
            print 'No output from the status script for worker ' + self.server
        self.status = result.output.split()[0]
        return self.status

    def upload_file(self,path):
        try:
            input_file = open(path,'r')
            shell = self.get_shell()
            with shell.open(self.scratch_dir + '/pj_worker_status_script.tmp', 'w') as remote_file:
                remote_file.write(unicode(input_file.read()))
            return True
        except spur.RunProcessError as e:
            error(e.stderr_output)
        except spur.NoSuchCommandError as e:
            error('could not find command: ' + e.command)
        except:
            error(str(sys.exc_info()[0]))
        return False