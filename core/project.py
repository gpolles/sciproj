__author__ = 'gpolles'
from logging import error
import datetime
from uuid import uuid4

class Project:
    def __init__(self, name, description='', db=None):
        self.db = db
        self.name = name
        self.uuid = unicode(uuid4())
        self.description = description
        self.date = unicode(datetime.datetime.now())
        self.parameters = []
        self.default_values = []
        self.branches = []

    def add_parameter(self, parameter, default_value):
        self.parameters.append(parameter)
        self.default_values.append(unicode(default_value))
        query = 'UPDATE projects SET parameters = ? , default_values = ? WHERE uuid = ?'
        self.db.execute(query, ('";"'.join(self.parameters),
                           '";"'.join(self.default_values),
                           self.uuid))
        self.db.commit()

    def add_branch(self, branch):
        if branch in self.branches:
            error('branch ' + branch + ' is already present.')
            return
        if len(branch) == 0:
            error('empty branch name')
            return
        self.branches.append(branch)
        query = 'UPDATE projects SET branches = ? WHERE uuid = ?'
        self.db.execute(query, ('";"'.join(self.branches), self.uuid))
        self.db.commit()