__author__ = 'gpolles'

import spur
from spur.ssh import escape_sh, _iteritems, _read_int_line, SshProcess


class MySshShell(spur.SshShell):
    def unsafe_run(self, *args, **kwargs):
        return self.unsafe_spawn(*args, **kwargs).wait_for_result()

    def unsafe_spawn(self, command, *args, **kwargs):
        new_process_group=False
        stdout = kwargs.pop("stdout", None)
        stderr = kwargs.pop("stderr", None)
        allow_error = kwargs.pop("allow_error", False)
        store_pid = kwargs.pop("store_pid", False)
        use_pty = kwargs.pop("use_pty", False)
        cwd = kwargs.pop('cwd',None)
        update_env = kwargs.pop('update_env',{})

        commands=[]
        #generate command without escaping
        if store_pid:
            commands.append("echo $$")

        if cwd is not None:
            commands.append("cd {0}".format(escape_sh(cwd)))

        update_env_commands = [
            "export {0}={1}".format(key, escape_sh(value))
            for key, value in _iteritems(update_env)
        ]
        commands += update_env_commands
        commands.append("echo $?")

        command = "{0}".format(command)
        if new_process_group:
            command = "setsid {0}".format(command)

        commands.append(command)
        command_in_cwd = "; ".join(commands)

        print command_in_cwd

        try:
            channel = spur.SshShell._get_ssh_transport(self).open_session()
        except EOFError as error:
            raise spur.SshShell._connection_error(self,error)
        if use_pty:
            channel.get_pty()
        channel.exec_command(command_in_cwd)

        process_stdout = channel.makefile('rb')

        if store_pid:
            pid = _read_int_line(process_stdout)

        process = SshProcess(
            channel,
            allow_error=allow_error,
            process_stdout=process_stdout,
            stdout=stdout,
            stderr=stderr,
            shell=self,
        )
        if store_pid:
            process.pid = pid

        return process