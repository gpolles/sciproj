__author__ = 'gpolles'

import uuid
import datetime
from copy import copy
# the script templates should use the syntax {!!key!!} which will be replaced by the corresponding
# value. {!!ID!!} is reserved for the id, which is also the name of the directory in which it will
# run.

# A good check script for checking running jobs, if one used screen, could be
#
# res=$(screen -ls | grep {!!ID!!})
# if [ -z "$res" ]
# then echo "completed"
# else echo "running"
# fi
#
# However, more complex output with checks on the success should be added.

# TODO: add parameters in the folder

# TODO: WORKER BY ID


class Job:

    def __init__(self):
        self.unique_id = unicode(uuid.uuid4())
        self.date = unicode(datetime.datetime.now())
        self.project = ''
        self.project_branch = ''
        self.template_script = ''
        self.template_status_script = ''
        self.status = ''
        self.remote_dir = ''
        self.candidate_workers = []
        self.is_running = False
        self.assigned_to = ''

        self.parameters = {'ID': str(self.unique_id)}

    def assign_to_worker(self, worker):
        self.assigned_to = worker

    def set_parameters(self, params):
        self.parameters = copy(params)
        self.parameters['ID'] = str(self.unique_id)


