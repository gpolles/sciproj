__author__ = 'gpolles'
from datetime import datetime


class Submission:
    def __init__(self):
        self.date = datetime.now()
        self.parameter_space = {}
        self.script = None
        self.workers = []
        self.jobs = []
