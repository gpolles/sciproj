__author__ = 'gpolles'
from core.job import Job
from core.submission import Submission
from logging import error,debug
import sqlite3


def get_combination(parameter_space, n):
    params = {}
    for i in range(len(parameter_space)):
        size = len(parameter_space[i][1])
        params[parameter_space[i][0]] = parameter_space[i][1][n % size]
        n = int(n/size)
    return params


class JobManager:
    def __init__(self):
        self.conn = sqlite3.connect('.jobs.db')
        sqlite3.register_adapter(bool, int)
        sqlite3.register_converter("BOOLEAN", lambda v: bool(int(v)))
        query = 'CREATE TABLE IF NOT EXISTS jobs (id INTEGER PRIMARY KEY, uuid TEXT, date TEXT, project TEXT, ' \
                'branch TEXT, script TEXT, status_script TEXT, ' \
                'candidate_workers TEXT, status TEXT, is_running INTEGER)'
        cur = self.conn.cursor()
        cur.execute(query)
        self.submissions = []

    def add_job(self, script, project, branch='', parameters=None, workers=None):
        if project is None:
            error('cannot add job to non specified project')
            return None
        if parameters is None:
            parameters = {}
        if workers is None:
            workers = []
        job = Job()
        job.set_parameters(parameters)
        job.project = project.name
        job.project_branch = branch
        job.template_script = script.unique_id
        for w in workers:
            job.candidate_workers.append(w.unique_id)
        candidate_workers_str = ';'.join(job.candidate_workers)
        job.status = 'queued'
        job.is_running = False
        cur = self.conn.cursor()
        values = (job.unique_id, job.date, job.project, job.project_branch, job.template_script,
                  job.template_status_script, candidate_workers_str, job.status, job.is_running)
        query = 'INSERT INTO jobs (uuid, date, project, branch, script, status_script, ' \
                'candidate_workers, status, is_running) VALUES (' + ','.join(['?']*len(values)) + ')'
        cur.execute(query, values)

        table = '_'.join([job.project, job.project_branch, 'jobs'])
        params = []
        # TODO: parameters should be 1-word and escaped
        for p,v in zip(project.parameters,project.default_values):
            params.append(p + ' TEXT DEFAULT ' + v)


        query = 'CREATE TABLE IF NOT EXISTS ' + table + '(id INTEGER PRIMARY KEY, job TEXT NOT NULL, ' \
                + ', '.join(params) + ')'
        cur.execute(query)

        query = 'INSERT into ' + table + ' ( job, ' + ', '.join(parameters) + ' ) VALUES ( ?,' \
                + ','.join(['?']*len(parameters)) + ')'
        cur.execute(query,[job.unique_id] + parameters.values())

        self.conn.commit()
        return job

    def submit_block(self, script, workers, project, branch, param_space):
        num_jobs = 1
        for p in param_space:
            num_jobs *= len(p[1])
        # generate jobs
        jobs = []
        for i in range(num_jobs):
            params = get_combination(param_space, i)
            j = self.add_job(script, project.name, branch=branch, parameters=params, workers=workers)
            jobs.append(j.unique_id)
        # save submission
        submission = Submission()
        submission.parameter_space = param_space
        submission.script = script
        submission.workers = workers
        submission.jobs = jobs
        self.submissions.append(submission)

    def get_first_queued_job(self, project=None, branch=''):
        cur = self.conn.cursor()
        query = 'SELECT TOP 1 (uuid, date, project, branch, script, status_script, ' \
                'candidate_workers) FROM jobs WHERE status = "queued" '
        if project is not None:
            query += ' AND project = "' + project.name + '" '
        if branch != '':
            query += ' AND branch = "' + branch + '"'
        cur.execute(query)
        j_r = cur.fetchone()
        if j_r is None:
            debug('get_first_queued_job: no objects found')
            return None
        job = Job()
        job.status = 'queued'
        (job.uuid, job.date, job.project, job.branch, job.template_script, job.template_status_script,
         job.candidate_workers) = j_r

        table = '_'.join([job.project, job.project_branch, 'jobs'])
        param_str = '(' + ','.join(project.parameters) + ')'
        query = 'SELECT ' + param_str + ' FROM ' + table + ' WHERE uuid = ' + job.unique_id
        cur.execute(query)
        p_r = cur.fetchone()
        if p_r is None:
            error('cannot find parameters for job ' + job.unique_id)
            return None
        for (p, v) in zip(job.parameters, p_r):
            job.parameters[p] = v
        return Job

