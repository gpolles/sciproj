__author__ = 'gpolles'

from core.worker import Worker
from logging import error
from uuid import UUID
import sqlite3
from simplecrypt import encrypt, decrypt
import pickle


class WorkerManager:
    def __init__(self):
        self.workers = []
        self.load_state()
        self.conn = sqlite3.connect('.sciprj.db')

    def add_worker(self, server, user, pwd, scratch_dir, status_script_path):
        w = Worker()
        w.server = server
        w.username = user
        w.password = pwd
        w.scratch_dir = scratch_dir
        if not self.load_status_script(w, status_script_path):
            error('could not load the status script.')
            return None
        if not w.upload_status_script():
            error('could not upload the status script.')
            return None
        self.workers.append(w)

        cur = self.conn.cursor()
        query = 'INSERT INTO workers (uuid,server,user,pwd,scratch,status_script,system) ' \
                'VALUES (?,?,?,?,?,?,?)'
        cur.execute(query,
                    (w.unique_id,
                    w.server,
                    w.username,
                    encrypt(Worker.encryption_pwd,pwd),
                    w.scratch_dir,
                    w.status_script,
                    w.system))
        self.conn.commit()
        self.conn.close()

    def load_state(self):
        cur = self.conn.cursor()
        query = 'CREATE TABLE IF NOT EXISTS workers (id INTEGER PRIMARY KEY, ' \
                'uuid TEXT, server TEXT, user TEXT, pwd TEXT, scratch TEXT, status_script TEXT,' \
                'system TEXT)'
        cur.execute(query)

        cur.execute('SELECT * FROM workers')

        r = cur.fetchone()
        while r is not None:
            (i,unique_id, server, user, pwd, scratch, status_script, system) = r
            w = Worker()
            w.unique_id = UUID(unique_id)
            w.server = str(server)
            w.username = str(user)
            w.pwd = decrypt(Worker.encryption_pwd,str(pwd))
            w.scratch_dir = str(scratch)
            w.status_script = str(status_script)
            w.system = str(system)
            self.workers.append(w)
            r = cur.fetchone()
        self.conn.close()

    def load_status_script(self, w, path):
        try:
            fin = open(path, 'r')
            w.status_script = fin.read()
            fin.close()
            return True
        except IOError as e:
            error('Error: Cannot load file ' + path + str(e))
            return False

    def del_worker(self, i):
        try:
            del self.workers[i]
            self.save_state()
        except IndexError:
            error('invalid worker id')

    def get_worker(self, i):
        try:
            return self.workers[i]
        except IndexError:
            error('invalid worker id')

    def get_worker_by_uuid(self, unique_id):
        for w in self.workers:
            if w.unique_id == unique_id:
                return w
        return None