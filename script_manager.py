__author__ = 'gpolles'

from logging import error
import pickle
import sqlite3
from core.script import Script
from uuid import UUID

class ScriptManager:
    def __init__(self):
        self.scripts = []
        self.conn = sqlite3.connect('.scripts.db')
        cur = self.conn.cursor()
        query = 'CREATE TABLE IF NOT EXISTS scripts (id INTEGER PRIMARY KEY, ' \
                'uuid TEXT NOT NULL, content TEXT, description TEXT, path TEXT, status_path TEXT, status_script TEXT, ' \
                'project TEXT, branch TEXT, script_class TEXT)'
        cur.execute(query)

        cur.execute('SELECT uuid,content,description,path,status_path,status_script,project,branch,script_class ' \
                    'FROM scripts')
        r = cur.fetchone()
        while r is not None:
            s = Script()
            (i, s.uuid, s.content, s.path, s.status_path, s.status_script, s.project, s.branch, s.script_class) = r
            r = cur.fetchone()

    # TODO: checks

    def add_script(self, path='', description='', status_path='', project='', branch='', script_class=''):
        s = Script(path=path, description=description, status_path=status_path, project=project,
                   branch=branch, script_class=script_class)
        self.scripts.append(s)

        cur = self.conn.cursor()
        query = 'INSERT INTO scripts ' \
                '(uuid,content,description,path,status_path,status_script,project,branch,script_class) ' \
                'VALUES (?,?,?,?,?,?,?,?,?)'
        cur.execute(query,
                    (s.uuid,
                     s.content,
                     s.description,
                     s.path,
                     s.status_path,
                     s.status_script,
                     s.project,
                     s.branch,
                     s.script_class))
        self.conn.commit()

    def del_script(self, i):
        try:
            uuid = self.scripts[i].uuid
            del self.scripts[i]
        except IndexError:
            error('invalid id')
            return
        query = 'DELETE FROM scripts WHERE uuid = ?'
        self.conn.execute(query,(uuid,))
        self.conn.commit()

    def get_script(self, i):
        try:
            return self.scripts[i]
        except IndexError:
            error('invalid id')

    def get_script_by_uuid(self, uuid):
        for s in self.scripts:
            if s.uuid == uuid:
                return s
        return None
