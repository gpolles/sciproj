from core.project import Project
from logging import error
import os
import sqlite3
import pickle


class ProjectManager:
    def __init__(self):
        self.projects = []
        self.active_project = None
        self.active_branch = None
        self.conn = sqlite3.connect('.projects.db')
        cur = self.conn.cursor()
        query = 'CREATE TABLE IF NOT EXISTS projects (id INTEGER PRIMARY KEY, ' \
                'uuid TEXT NOT NULL, name TEXT, description TEXT, date TEXT, parameters TEXT, default_values TEXT, ' \
                'branches TEXT)'
        cur.execute(query)

        cur.execute('SELECT name,description,date,parameters,default_values,branches,uuid FROM projects')
        r = cur.fetchone()
        while r is not None:
            (name, description, date, parameters, default_values, branches, uuid) = r
            p = Project(name, description, db=self.conn)
            p.uuid = uuid
            p.date = date
            p.parameters = parameters.split('";"')
            p.default_values = default_values.split('";"')
            p.branches = branches.split('";"')
            r = cur.fetchone()
            self.projects.append(p)

        self.load_state()


    def add_project(self, name, description=''):
        if name == '':
            error('add_project(): must specify project name')
            return None
        for p in self.projects:
            if p.name == name:
                error('Error: Name already in use.')
                return None
        # TODO: check if directory exists and handle exception
        try:
            os.makedirs(name,mode=0755)
        except OSError as e:
            error('cannot create project dir %s ' % e)

        p = Project(name, description, db=self.conn)
        self.projects.append(p)
        query = 'INSERT INTO projects (uuid,name,description,date,parameters,default_values,branches) VALUES ' \
                '(?,?,?,?,?,?,?)'
        self.conn.execute(query,
                          (p.uuid,
                           p.name,
                           p.description,
                           p.date,
                           '";"'.join(p.parameters),
                           '";"'.join(p.default_values),
                           '";"'.join(p.branches)))
        self.conn.commit()
        return p

    def select_project(self, i):
        try:
            self.active_project = self.projects[i]
        except IndexError:
            error('Invalid project id.')

    def save_state(self):
        try:
            with open('.status.pickle','w') as f:
                pickle.dump((self.active_project.uuid, self.active_branch), f)
        except:
            error('cannot save state')

    def load_state(self):
        try:
            with open('.status.pickle','r') as f:
                (active_project_uuid, self.active_branch) = pickle.load(f)
                self.active_project = self.get_project_by_uuid(active_project_uuid)
        except:
            error('cannot load state')

    def get_project_by_uuid(self, uuid):
        for p in self.projects:
            if p.uuid == uuid:
                return p
        return None

